# LDAPcontrol UserSync
Some customizing is done via $conf in settings.php
Some functions are only available with drush:
http://drupal.org/project/drush

## Main Features
- Storing user related setting and LDAP saltet and hashed password in Drupal DB.
- Drupal DB entry is created and modified on user_update.
- Drupal DB entry is deleted on user_delete.
- Standard LDAP entry operation can be disabled via settings.php.
- Individual LDAP operations can be done via hook_ldapcontrol_usersync_update().

## Standard LDAP entry
- Standard LDAP entry is based on objectClass "inetOrgPerson"
- Saltet and hashed password is stored in standard attribute "userPassword"
- Standard LDAP entry is deleted when drupal user is deleted.
- Standard LDAP entry is created/updated or deleted on hook_user_update.
- Text field API can be mapped to LDAP attributes "SN" and "givenName".
- Drupal mail address can optional be added as standard attribute "mail" 
- Uses LDAP attribute "uid" to map drupal uid in standard LDAP entry.
- Select LDAP attribute for building "DN" together with "base"-setting.
