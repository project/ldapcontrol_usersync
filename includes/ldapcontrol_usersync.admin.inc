<?php

/**
 * @file
 * LDAPcontrol User Sync admin page callback.
 */

/**
 * Form builder function.
 */
function ldapcontrol_usersync_settings_form() {
  global $conf;
  if (!isset($conf['ldapcontrol_usersync_base'])
    || ($conf['ldapcontrol_usersync_base'] != 'none')) {
    $t = t("Warning: Setting 'ldapcontrol_usersync_base' isn't defined in settings.php. If you don't want to use the LDAP operation of this module e.g. for using hook_ldapcontrol_usersync_update() in a custom module you can disable with value 'none'.");
    drupal_set_message($t, 'warning');
  }

  $form['ldapcontrol_usersync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable LDAPcontrol User Sync'),
    '#description' => t("WARNING: Only disable on testing situations because no User Sync is proceeded when disabled. Especially synchronized passwords can not be restored."),
    '#default_value' => variable_get('ldapcontrol_usersync', 0),
  );

  $form['ldapcontrol_usersync_pwsync_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Password synchronization enabled by default.'),
    '#description' => t("What's the default setting for users profiles."),
    '#default_value' => variable_get('ldapcontrol_usersync_pwsync_default', 0),
  );

  return system_settings_form($form);
}
